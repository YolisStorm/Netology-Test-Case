import AndroidDefaultConfig.COMPILE_SDK_VERSION
import AndroidDefaultConfig.ID
import AndroidDefaultConfig.MIN_SDK_VERSION
import AndroidDefaultConfig.TARGET_SDK_VERSION
import AndroidDefaultConfig.TEST_INSTRUMENTATION_RUNNER
import AndroidDefaultConfig.VERSION_CODE
import AndroidDefaultConfig.VERSION_NAME

plugins {
    id(GradlePluginId.ANDROID_APPLICATION)
    kotlin("android")
    id("com.github.ben-manes.versions")
}

android {
    compileSdk = COMPILE_SDK_VERSION

    defaultConfig {
        applicationId = ID
        minSdk = MIN_SDK_VERSION
        targetSdk =  TARGET_SDK_VERSION
        versionCode = VERSION_CODE
        versionName = VERSION_NAME

        testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER

    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName(BuildType.DEBUG) {
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
        android.buildFeatures.dataBinding = true
    }
    dynamicFeatures += setOf(ModuleDependency.FeatureThemesList)
}

dependencies {

    api(project(ModuleDependency.LibraryCommon))

    implementation(LibraryDependencies.Koin.Android)

    api(LibraryDependencies.AndroidSupport.Design.ConstraintLayout)
    api(LibraryDependencies.AndroidSupport.Design.Material)

    api(LibraryDependencies.Navigation.FragmentKtx)
    api(LibraryDependencies.Navigation.DynamicFeature)

    addTestDependencies()
}

fun getDynamicFeatureModuleNames() = ModuleDependency.getDynamicFeatureModules()
    .map { it.replace(":features:", "") }
    .toSet()
