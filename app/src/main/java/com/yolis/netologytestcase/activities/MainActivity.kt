package com.yolis.netologytestcase.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.yolis.netologytestcase.R
import com.yolis.netologytestcase.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, getString(R.string.fab_action_text), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show()
        }

        setContentView(binding.root)
    }
}