package com.yolis.netologytestcase

import android.app.Application
import com.google.android.play.core.splitcompat.SplitCompat
import com.google.android.play.core.splitcompat.SplitCompatApplication
import com.yolis.netologytestcase.helpers.ReleaseTimberTree
import timber.log.Timber
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE

class MainApplication : SplitCompatApplication() {

    override fun onCreate() {
        super.onCreate()
        SplitCompat.install(this)
        configureTimber()
        configureKoin()
    }

    private fun configureTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        else
            Timber.plant(ReleaseTimberTree())
    }

    private fun configureKoin() {
        startKoin {
            logger(object : Logger() {
                override fun log(level: Level, msg: MESSAGE) {
                    if (BuildConfig.DEBUG)
                        when (level) {
                            Level.DEBUG -> Timber.d(msg)
                            Level.ERROR -> Timber.e(msg)
                            Level.INFO -> Timber.i(msg)
                            Level.NONE -> Timber.w(msg)
                        }
                }
            })
            androidContext(this@MainApplication)
        }
    }

}