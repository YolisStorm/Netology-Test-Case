package com.yolis.netologytestcase.helpers

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

/**
 * Класс для обработки логов и занесения их в аналитику
 */
class ReleaseTimberTree : Timber.Tree() {

	override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
		when (priority) {
			Log.ERROR -> {
				Crashlytics.logException(t)
			}
			Log.WARN -> {
				Crashlytics.log(priority, tag, message)
			}
		}
	}
}