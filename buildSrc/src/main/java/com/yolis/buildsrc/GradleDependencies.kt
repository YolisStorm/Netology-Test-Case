import org.gradle.api.artifacts.dsl.RepositoryHandler
import java.net.URI

object GradlePluginVersion {
    const val ANDROID_GRADLE                =   "7.3.0-alpha07"
    const val KTLINT_GRADLE                 =   "10.2.1"
    const val KOTLIN                        =   CoreVersion.KOTLIN
    const val GRADLE_UPDATER_VERSION_PLUGIN =   "0.42.0"
    const val SAFE_ARGS                     =   CoreVersion.NAVIGATION
    const val GOOGLE_GMS                    =   CoreVersion.GMS_GOOGLE_SERVICES
    const val CRASHLYTICS_GRADLE            =   CoreVersion.CRASHLYTICS_GRADLE
}

object GradlePluginId {
    const val ANDROID_APPLICATION           =   "com.android.application"
    const val ANDROID_DYNAMIC_FEATURE       =   "com.android.dynamic-feature"
    const val ANDROID_LIBRARY               =   "com.android.library"
    const val KOTLIN_JVM                    =   "org.jetbrains.kotlin.jvm"
    const val KOTLIN_ANDROID                =   "org.jetbrains.kotlin.android"
    const val KOTLIN_ANDROID_EXTENSIONS     =   "org.jetbrains.kotlin.android.extensions"
    const val KOTLIN_SERIALIZATION          =   "org.jetbrains.kotlin.serialization"
    const val KOTLIN_KAPT                   =   "kotlin-kapt"
    const val SAFE_ARGS                     =   "androidx.navigation.safeargs.kotlin"
}

object GradleOldWayPlugins {
    const val ANDROID_GRADLE                =   "com.android.tools.build:gradle:${GradlePluginVersion.ANDROID_GRADLE}"
    const val KOTLIN_GRADLE                 =   "org.jetbrains.kotlin:kotlin-gradle-plugin:${GradlePluginVersion.KOTLIN}"
    const val GOOGLE_GMS_SERVICES           =   "com.google.gms:google-services:${GradlePluginVersion.GOOGLE_GMS}"
    const val FIREBASE_CRASHLYTICS          =   "com.google.firebase:firebase-crashlytics-gradle:${GradlePluginVersion.CRASHLYTICS_GRADLE}"
    const val SAFE_ARGS                     =   "androidx.navigation:navigation-safe-args-gradle-plugin:${GradlePluginVersion.SAFE_ARGS}"
    const val GRADLE_UPDATER                =   "com.github.ben-manes:gradle-versions-plugin:${GradlePluginVersion.GRADLE_UPDATER_VERSION_PLUGIN}"
}

fun addRepos(handler : RepositoryHandler) {
    handler.google()
    handler.gradlePluginPortal()
}