// Versions consts that are used across libraries and Gradle plugins
object CoreVersion {
    const val COROUTINES_ANDROID            =   "1.6.0"
    const val KOIN                          =   "3.1.5"
    const val KOTLIN                        =   "1.6.10"
    const val KOTLIN_CORE                   =   "1.6.10"
    const val KOTLIN_REFLECTION             =   "1.6.10"
    const val NAVIGATION                    =   "2.5.0-alpha03"
    const val GMS_GOOGLE_SERVICES           =   "4.3.3"
    const val CRASHLYTICS_GRADLE            =   "2.0.0"
}