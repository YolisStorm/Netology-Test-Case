package com.yolis.themeslist.fragments.listadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yolis.network.dto.Direction
import com.yolis.themeslist.databinding.ViewHolderItemThemeBinding

/**
 * Контейнер для отображения тематики
 */
class ThemeViewHolder
private constructor(private val binding: ViewHolderItemThemeBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Direction) {
        binding.themeItem = item
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup) : ThemeViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ViewHolderItemThemeBinding.inflate(layoutInflater, parent, false)
            return ThemeViewHolder(binding)
        }
    }

}