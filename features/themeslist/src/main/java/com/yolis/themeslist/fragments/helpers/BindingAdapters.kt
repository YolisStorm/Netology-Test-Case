package com.yolis.themeslist.fragments

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter

/**
 * Адаптер для изменения цвета элемента по входной строке формата #FFF или #FFFFFF
 */
@BindingAdapter("tintFromHexString")
fun ImageView.setTintFromHexString(color: String) {
    val colorToParse = if (color.length == 4) { // #XXX
        color.replace(Regex("#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])"), "#$1$1$2$2$3$3");
    } else color
    setColorFilter(Color.parseColor(colorToParse))
}

/**
 * Адаптер для изменения видимости элемента по boolean значению
 */
@BindingAdapter("booleanVisibility")
fun View.setBooleanVisibility(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}
