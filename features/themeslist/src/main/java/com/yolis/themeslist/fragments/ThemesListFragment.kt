package com.yolis.themeslist.fragments

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.yolis.network.networkKoinModule
import com.yolis.themeslist.R
import com.yolis.themeslist.databinding.FragmentThemesListScreenLayoutBinding
import com.yolis.themeslist.fragments.listadapter.SummaryThemesAdapter
import com.yolis.themeslist.fragments.listadapter.ThemesItemDividerDecoration
import org.koin.core.context.loadKoinModules
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Фрагмент для отображения списка тем с заголовком
 */
class ThemesListFragment : Fragment() {

    private val loadModules by lazy {
        loadKoinModules(listOf(
            networkKoinModule,
            summaryKoinModule
        ))
    }
    private fun injectModules() = loadModules

    private lateinit var binding : FragmentThemesListScreenLayoutBinding

    override fun onCreateView(inflater: LayoutInflater,
                          container: ViewGroup?,
                          savedInstanceState: Bundle?
    ): View? {

        injectModules()

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_themes_list_screen_layout,
            container,
            false
        )

        val viewModel: ThemesListViewModel by viewModel()

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        lifecycleScope.launchWhenCreated {
            viewModel.updateThemesList()
        }

        val mainTitleText = getString(R.string.main_screen_title)
        //Добавление цвета к заголовку на основе HTML разметки
        binding.title.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(mainTitleText, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(mainTitleText)
        }

        //Настройка адаптера для списка
        binding.themesList.apply {
            LinearLayoutManager(context).run {
                isSoundEffectsEnabled = true
                layoutManager = this
            }
            SummaryThemesAdapter().also { themesAdapter ->
                themesAdapter.hasStableIds()
                adapter = themesAdapter
                //Настройка наблюдения за обновлениями списка тем
                viewModel.themesList.observe(viewLifecycleOwner) { list ->
                    if (list?.isNotEmpty() == true)
                        themesAdapter.submitList(list)
                }
            }
            //Добавление разделителя между элементами
            addItemDecoration(
                ThemesItemDividerDecoration(context, R.drawable.divider_drawable,
                    showFirstDivider = true,
                    showLastDivider = true
                )
            )
        }

        return binding.root
    }
}