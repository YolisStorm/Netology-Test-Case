package com.yolis.themeslist.fragments

import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val summaryKoinModule = module {
    viewModel {
        ThemesListViewModel(
            get(),
            androidApplication()
        )
    }
}