package com.yolis.themeslist.fragments.listadapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yolis.network.dto.Direction

/**
 * Адаптер для списка с темами
 */
class SummaryThemesAdapter() : ListAdapter<Direction, RecyclerView.ViewHolder>(ThemesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ThemeViewHolder.from(parent)


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ThemeViewHolder -> holder.bind(getItem(position))
        }
    }

    override fun getItemId(position: Int): Long =
        getItem(position).itemId.mostSignificantBits and Long.MAX_VALUE
}

class ThemesDiffCallback : DiffUtil.ItemCallback<Direction>() {
    override fun areItemsTheSame(oldItem: Direction, newItem: Direction): Boolean =
        oldItem.itemId == newItem.itemId


    override fun areContentsTheSame(oldItem: Direction, newItem: Direction): Boolean =
        oldItem == newItem
}