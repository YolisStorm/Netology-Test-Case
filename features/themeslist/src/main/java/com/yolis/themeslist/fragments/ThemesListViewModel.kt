package com.yolis.themeslist.fragments

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yolis.network.dto.Direction
import com.yolis.network.helpers.resultWrappers.NetworkResultWrapper
import com.yolis.network.interfaces.INetologyThemesService

class ThemesListViewModel (
    private val netologyThemesService: INetologyThemesService,
    application: Application
) : AndroidViewModel(application) {

    /**
     * Список тематик
     */
    val themesList : LiveData<List<Direction>?>
        get() = _themesList

    /**
     * Флаг об ошибке при загрузке тематик
     */
    val isError : LiveData<Boolean>
        get() = _isError

    /**
     * Флаг об процессе загрузки тематик
     */
    val isLoading : LiveData<Boolean>
        get() = _isLoading

    /**
     * Метод для инициирования процесса загрузки тематик
     */
    suspend fun updateThemesList() {
        _isLoading.postValue(true)
        when (val result = netologyThemesService.getListOfThemes()) {
            is NetworkResultWrapper.Success -> {
                _themesList.postValue(result.value)
                _isLoading.postValue(false)
                if (result.value.isEmpty()) {
                    _isError.postValue(true)
                }
            }
            else -> {
                _isLoading.postValue(false)
                _isError.postValue(true)
            }
        }
    }

    private val _themesList = MutableLiveData<List<Direction>?>()
    private val _isError = MutableLiveData<Boolean>(false)
    private val _isLoading = MutableLiveData<Boolean>(false)

}