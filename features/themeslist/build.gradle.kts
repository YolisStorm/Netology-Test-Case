import AndroidDefaultConfig.COMPILE_SDK_VERSION
import AndroidDefaultConfig.MIN_SDK_VERSION
import AndroidDefaultConfig.TEST_INSTRUMENTATION_RUNNER

plugins {
    id(GradlePluginId.ANDROID_DYNAMIC_FEATURE)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    kotlin("kapt")
}
android {
    compileSdk = COMPILE_SDK_VERSION

    defaultConfig {
        minSdk = MIN_SDK_VERSION
        testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    viewBinding {
        android.buildFeatures.viewBinding = true
        android.buildFeatures.dataBinding = true
    }
}

dependencies {
    implementation(project(ModuleDependency.app))
    implementation(project(ModuleDependency.DataSourceNetwork))

    implementation(LibraryDependencies.Koin.Android)

    addTestDependencies()
}