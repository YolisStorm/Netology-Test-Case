import AndroidDefaultConfig.BUILD_TOOLS_VERSION
import AndroidDefaultConfig.COMPILE_SDK_VERSION
import AndroidDefaultConfig.MIN_SDK_VERSION
import AndroidDefaultConfig.TARGET_SDK_VERSION
import AndroidDefaultConfig.TEST_INSTRUMENTATION_RUNNER
import AndroidDefaultConfig.VERSION_CODE
import AndroidDefaultConfig.VERSION_NAME

plugins {
    id(GradlePluginId.ANDROID_LIBRARY)
    id(GradlePluginId.KOTLIN_ANDROID)
    kotlin("kapt")
}

android {
    compileSdk = COMPILE_SDK_VERSION
    buildToolsVersion = BUILD_TOOLS_VERSION

    defaultConfig {
        minSdk = MIN_SDK_VERSION
        targetSdk =  TARGET_SDK_VERSION
        testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER
    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName(BuildType.DEBUG) {
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }
}

dependencies {

    api(LibraryDependencies.Kotlin.Core)
    api(LibraryDependencies.Kotlin.Reflection)
    api(LibraryDependencies.Kotlin.Coroutines.Android)

    api(LibraryDependencies.Main.Gson)

    api(LibraryDependencies.AndroidSupport.PlayCore)

    api(LibraryDependencies.Main.Timber)
    api(LibraryDependencies.AndroidSupport.AppCompat)
    api(LibraryDependencies.AndroidSupport.CoreKtx)
    api(LibraryDependencies.AndroidSupport.Fragment.FragmentRuntimeKtx)

    api(LibraryDependencies.AndroidSupport.Design.Material)

    api(LibraryDependencies.Navigation.RuntimeKtx)

    api(LibraryDependencies.Lifecycle.Extensions)
    api(LibraryDependencies.Lifecycle.LivedataKtx)
    api(LibraryDependencies.Lifecycle.ViewModelKtx)

    api(LibraryDependencies.Firebase.Crashlytics)
    api(LibraryDependencies.Firebase.CommonKtx)

}