import AndroidDefaultConfig.COMPILE_SDK_VERSION
import AndroidDefaultConfig.MIN_SDK_VERSION
import AndroidDefaultConfig.TARGET_SDK_VERSION
import AndroidDefaultConfig.TEST_INSTRUMENTATION_RUNNER

plugins {
    id(GradlePluginId.ANDROID_LIBRARY)
    id(GradlePluginId.KOTLIN_ANDROID)
}

android {
    compileSdk = COMPILE_SDK_VERSION

    defaultConfig {
        minSdk = MIN_SDK_VERSION
        targetSdk =  TARGET_SDK_VERSION
        testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER


        buildConfigField("String", "API_BASE_URL", "\"https://raw.githubusercontent.com\"")
    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName(BuildType.DEBUG) {
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {

    implementation(project(ModuleDependency.LibraryCommon))

    api(LibraryDependencies.Main.Timber)

    api(LibraryDependencies.Kotlin.Coroutines.Android)
    api(LibraryDependencies.AndroidSupport.CoreKtx)

    implementation(LibraryDependencies.Koin.Core)

    implementation(LibraryDependencies.Main.Gson)
    implementation(LibraryDependencies.Okhttp.Main)
    implementation(LibraryDependencies.Okhttp.LoggingInterceptor)
    implementation(LibraryDependencies.Retrofit.Runtime)
    implementation(LibraryDependencies.Retrofit.Converters.Gson)

    addTestDependencies()
}