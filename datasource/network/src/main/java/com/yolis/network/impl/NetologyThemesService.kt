package com.yolis.network.impl

import com.yolis.network.dto.Direction
import com.yolis.network.helpers.resultWrappers.NetworkResultWrapper
import com.yolis.network.helpers.safeApiCall
import com.yolis.network.interfaces.INetologyThemesService
import com.yolis.network.raw_api.INetologyThemesApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Реализация сервиса по получению тематик
 *
 * @param netologyThemesApi описание запросов
 * @param dispatcher указание в каком потоке стоит исполнять запрос
 */
class NetologyThemesService private constructor(
    private val netologyThemesApi: INetologyThemesApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : INetologyThemesService {

    /**
     * Метод для получения набора тематик
     */
    override suspend fun getListOfThemes(): NetworkResultWrapper<List<Direction>> =
        safeApiCall(dispatcher) {
            netologyThemesApi.getThemes().data.map {
                it.direction
            }
        }

    companion object {
        @Volatile
        private var instance: INetologyThemesService? = null

        /**
         * Метод для создания экземпляра сервиса
         */
        fun getInstance(
            netologyThemesApi: INetologyThemesApi,
            dispatcher: CoroutineDispatcher = Dispatchers.IO
        ) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: NetologyThemesService(
                            netologyThemesApi,
                            dispatcher
                        ).also { instance = it }
                }
    }
}