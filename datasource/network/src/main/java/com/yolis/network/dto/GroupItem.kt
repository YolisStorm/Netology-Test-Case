package com.yolis.network.dto

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Объект информации по группе
 *
 * @param itemId ID группы
 * @param link ссылка для получения доп. информации
 * @param badge объект значка
 * @param items коллекция информации по группе
 * @param title название группы
 */
data class GroupItem(
    @SerializedName("id")
    val itemId: UUID,
    @SerializedName("link")
    val link: String?,
    @SerializedName("badge")
    val badge: Badge,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("title")
    val title: String
)
