package com.yolis.network

import com.yolis.network.helpers.RetrofitFactory
import com.yolis.network.impl.NetologyThemesService
import com.yolis.network.interfaces.INetologyThemesService
import com.yolis.network.raw_api.INetologyThemesApi
import org.koin.dsl.module

val networkKoinModule = module {
    single<INetologyThemesApi> {
        RetrofitFactory
            .retrofit(BuildConfig.API_BASE_URL)
            .create(INetologyThemesApi::class.java)
    }
    single<INetologyThemesService> {
        NetologyThemesService.getInstance(get())
    }
}