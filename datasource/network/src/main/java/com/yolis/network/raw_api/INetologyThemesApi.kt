package com.yolis.network.raw_api

import com.yolis.network.dto.MainObject
import retrofit2.http.GET

/**
 * Описание запросов для получения набора тематик
 */
interface INetologyThemesApi {

    @GET("/netology-code/rn-task/master/netology.json")
    suspend fun getThemes(): MainObject

}