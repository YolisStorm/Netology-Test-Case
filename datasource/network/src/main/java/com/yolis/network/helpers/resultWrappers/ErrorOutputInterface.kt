package com.yolis.network.helpers.resultWrappers

import java.lang.Exception

abstract class ErrorOutputInterface : Exception()