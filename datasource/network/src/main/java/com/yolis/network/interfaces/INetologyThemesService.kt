package com.yolis.network.interfaces

import com.yolis.network.dto.Direction
import com.yolis.network.helpers.resultWrappers.NetworkResultWrapper

/**
 * Описание структуры сервиса по получению набора тематик
 */
interface INetologyThemesService {

    suspend fun getListOfThemes() : NetworkResultWrapper<List<Direction>>

}