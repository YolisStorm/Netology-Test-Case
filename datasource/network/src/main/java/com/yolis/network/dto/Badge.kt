package com.yolis.network.dto

import android.graphics.Color

/**
 * Объект Значка
 *
 * @param text текст
 * @param color цвет значка
 * @param bgColor цвет фона значка
 */
data class Badge(
    val text: String?,
    val color: String,
    val bgColor: String
)
