package com.yolis.network.helpers.resultWrappers

import com.google.gson.annotations.SerializedName

data class BadRequestOutput(
	@SerializedName("error")
	val badRequestCause: String,
	@SerializedName("error_description")
	val description: String
) : ErrorOutputInterface() {
	override fun toString(): String =
		"Cause - $badRequestCause | Description - $description"
}
