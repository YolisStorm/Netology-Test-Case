package com.yolis.network.dto

import com.google.gson.annotations.SerializedName

/**
 * Объект с данными об направлении и группах
 *
 * @param groups список групп
 * @param direction описание направления
 */
data class DataItem(
    @SerializedName("groups")
    val groups: List<GroupItem>,
    @SerializedName("direction")
    val direction: Direction
)
