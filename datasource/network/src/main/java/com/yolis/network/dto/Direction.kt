package com.yolis.network.dto

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Объект направления курса
 *
 * @param itemId ID направления
 * @param link ссылка для перехода к доп. информации
 * @param badge объект значка
 * @param title название направления
 */
data class Direction(
    @SerializedName("id")
    val itemId: UUID,
    @SerializedName("link")
    val link: String?,
    @SerializedName("badge")
    val badge: Badge,
    @SerializedName("title")
    val title: String
)
