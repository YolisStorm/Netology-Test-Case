package com.yolis.network.dto

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Объект базовых данных
 *
 * @param itemId ID элемента
 * @param link ссылка для получения доп. информации
 * @param badge объект значка
 * @param title заголовок с названием
 */
data class Item(
    @SerializedName("id")
    val itemId: UUID,
    @SerializedName("link")
    val link: String?,
    @SerializedName("badge")
    val badge: Badge,
    @SerializedName("title")
    val title: String
)
