package com.yolis.network.dto

import com.google.gson.annotations.SerializedName

/**
 * Главный объект необходимый для получения данных
 *
 * @param data список с данными
 */
data class MainObject(
    @SerializedName("data")
    val data: List<DataItem>
)
