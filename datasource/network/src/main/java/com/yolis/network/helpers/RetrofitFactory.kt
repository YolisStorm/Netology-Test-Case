package com.yolis.network.helpers

import com.google.gson.GsonBuilder
import com.yolis.network.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Фабрика для создания обработчика запросов с сервера
 */
internal object RetrofitFactory {

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val client =
        if (BuildConfig.DEBUG) {
            with (OkHttpClient().newBuilder()) {
                addInterceptor(loggingInterceptor)
                build()
            }
        } else {
            OkHttpClient()
                .newBuilder()
                .build()
        }

    fun retrofit(baseUrl: String): Retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder()
                    .create()
            )
        )
        .build()
}