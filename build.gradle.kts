// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    addRepos(repositories)
    dependencies {
        classpath(GradleOldWayPlugins.ANDROID_GRADLE)
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
        classpath(GradleOldWayPlugins.GRADLE_UPDATER)
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

task<Delete>("clean"){
    delete(rootProject.buildDir)
}